import java.io.*;
import java.util.*;

/* Classe responsavel pela execucao da maquina virtual
para um determinado ficheiro C--
*/
public class ThExec extends Thread {
	private String fichInput;

	public ThExec(String input) {
		this.fichInput = input;
	}

	public void run() {
		try {
			String path = new StringBuilder().append(System.getProperty("user.dir")).toString();
			String pathDir = new StringBuilder().append(System.getProperty("user.dir")).append("/MSP/genMaqV").toString();
			File dir = new File(pathDir);

			Runtime rtime = Runtime.getRuntime();

			//abertura da shell 
			Process child = rtime.exec("/bin/bash", null, dir); 
			BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));

			outCommand.write(new StringBuilder().append("export CLASSPATH=.:${CLASSPATH}:").append(path).append("/JarFiles/tom/lib/tom-runtime-full.jar\n").toString());
			outCommand.write(new StringBuilder().append("export PATH=.:${PATH}:").append(path).append("/JarFiles/tom/bin\n").toString());
			
			if (this.fichInput == null) {
				outCommand.write("java maqv/Main res.msp \n");
			} else {
				outCommand.write("java maqv/Main res.msp < ../../"+this.fichInput+" \n");
			}

			//fecho da shell
			outCommand.write("exit \n");
			outCommand.flush();

			outCommand.close();
			//int wait = child.waitFor();

			BufferedReader reader = new BufferedReader(new InputStreamReader(child.getInputStream()));

            String line = "";			
			while ((line = reader.readLine())!= null) {
				System.out.println(line);
			}
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
