#!/bin/sh -e
EXAMPLE_DIR=exemplos/
EXAMPLE_FILE=fat_todos.i
RES_FILE=res.msp

export CLASSPATH=.:${CLASSPATH}:/home/anasousa/Dropbox/MEI/MFES_ATS/Trabalho/GreenProfiling/JarFiles/tom/lib/tom-runtime-full.jar
export PATH=.:${PATH}:/home/anasousa/Dropbox/MEI/MFES_ATS/Trabalho/GreenProfiling/JarFiles/tom/bin
make clean
make all
cp $EXAMPLE_DIR$EXAMPLE_FILE genI
cd genI
javac gram/Main.java
java gram/Main < $EXAMPLE_FILE > $RES_FILE

cp $RES_FILE ../genMaqV
cd ../genMaqV
javac maqv/Main.java
echo "------------Programa em execucao----------------"
java maqv/Main $RES_FILE < ../exemplos/in.txt