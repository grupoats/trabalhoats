void main() {
	int num;
	num = input(int);
	int resR;
	resR = fibonacciR(num);
	print(resR);
} 

int fibonacciR(int num)
{
 	if(num==1 || num==2) {
   	   return 1;
   	}
   	else {
   		int f1 = fibonacciR(num-1);
   		int f2 = fibonacciR(num-2);
    	return f1+f2; 
   	}
}