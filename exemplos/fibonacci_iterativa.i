void main() {
	int num;
	num = input(int);
	int resI;
	resI = fibonacciI(num);
	print(resI);
} 

int fibonacciI(int num)                             
{                                          
	int i, resI1 = 1, resI2 = 1, soma;         
	for (i = 3; i <= num; i++)           
	{                                        
	soma = resI1 + resI2;                    
	resI1 = resI2;                           
	resI2 = soma;                           
	}                                        
	return resI2;                             
}                   