void main() {
	int num;
	num = input(int);
	int resI, resLU;

	resI = fatorialI(num);
	resLU = fatorialLU(num);
	print(resI);
	print(resLU);
}

//versao iterativa normal
int fatorialI(int num){
	int fat;
	for(fat = 1; num > 1; num--){
		fat = fat * num;
	}
	return fat;
}

//versao iterativa com loop unrolling
int fatorialLU(int num){
	int fat;
	fat = 1;
	while (num > 1) {
		fat = fat * num;
		num = num - 1;
		fat = fat * num;
		num = num - 1;
	}
	return fat;
}