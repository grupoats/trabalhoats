void main() {
	int num;
	num = read();
	int resI; int resR;
	resI = fatorialI(num);
	resR = fatorialR(num);
	print(resI);
	print(resR);
}


int fatorialI(int num){
	int fat;
	for(fat = 1; num > 1; num--){
		fat = fat * num;
	}
	return fat;
}

int fatorialR(int num){
	if (num==1 || num==0){
		return 1;
	}else{
		return num*fatorialR(num-1);
		
	}
}


//Loop Unrolling
int fatorialR(int num){
	int a, b;
	if (num<4){
		return 1;
	}else{
		a = num*fatorialR(num-1);
		b = (num-2)*fatorialR(num-3);
		return a*b;
	}
}


