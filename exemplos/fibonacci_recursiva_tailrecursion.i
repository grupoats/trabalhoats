void main() {
	int num;
	num = read();
	int resTR;
	resTR = fibonacciTR(num);
	print(resTR);
} 

//recursiva tail recursion
int fibonacciTRBody(int num, int latest, int older) {
	
	beggining:
	if (num==1 || num==2){
		return latest;
	}else{
		int soma = latest + older;
		older = latest;
		latest = soma;

		num--;

		goto beggining;
	}
}

int fibonacciTR(int num){
	fibonacciTRBody(num, 1, 1);
}