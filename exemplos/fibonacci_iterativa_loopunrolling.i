void main() {
	int num;
	num = input(int);
	int resLI;
	resLI = fibonacciLU(num);
	print(resLI);
} 

//iterativa loop unrolling
int fibonacciLU(int num){
	int i, resI1 = 1, resI2 = 1, soma1, soma2;   

	//tratar  impares
	if((num >= 3) && (num % 2 != 0)){
		soma2 = resI1 + resI2;                    
		resI1 = resI2;                           
		resI2 = soma2;
	}

	for (i = 3; i < num; i = i + 2) {                                        
		soma1 = resI1 + resI2;
		soma2 = resI2 + soma1;                    

		resI1 = soma1;                           
		resI2 = soma2;                           
	}

	return resI2;        
}