int i;

void main ()
{
  	normal(10);
  	opt(10);
  	print('a');
}

void normal(int a) {
	int x, i;
	i = 0;
	for(x=0; x<a; x++) {
		i+=1;
  		i+=1;
	}
}

//optimizacao Instruction Combining
void opt(int a) {
	int x, i;
	i = 0;
	for(x=0; x<a; x++) {
		i+=2;
	}
}