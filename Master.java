/* Classe responsavel pelo controlo das threads que executam 
a maquina virtual e as medicoes do consumo energetico
*/

import java.io.*;

public class Master {
	
	public static void main(String[] args) throws InterruptedException {
		File fich = new File("MSP/maqv/actualFuncName.txt");
		String fichCompilar = null;
		String inputFichComp = null;
		String pathFich = null;

		if (args.length < 2) {
			System.out.println("ERRO: Indique o nome e/ou caminho do ficheiro a executar!");
		} else {
			fichCompilar = args[0];
			pathFich = args[1];

			if (args.length > 2) {
				inputFichComp = args[2];
			}

			try {
				//criacao ficheiro para guardar o nome da funcao que esta a ser executada na MaqV
	            fich.createNewFile();
	            fich.setWritable(true);
	            fich.setReadable(true);
	            fich.setExecutable(true);

				String path = new StringBuilder().append(System.getProperty("user.dir")).toString();
				String pathDir = new StringBuilder().append(System.getProperty("user.dir")).append("/MSP").toString();
				File dir = new File(pathDir);

				Runtime rtime = Runtime.getRuntime();

				/* Compilacao MaqV e programa C-- */
				//abertura da shell 
				Process child = rtime.exec("/bin/bash", null, dir); 
				BufferedWriter outCommand = new BufferedWriter(new OutputStreamWriter(child.getOutputStream()));

				outCommand.write(new StringBuilder().append("export CLASSPATH=.:${CLASSPATH}:").append(path).append("/JarFiles/tom/lib/tom-runtime-full.jar\n").toString());
				outCommand.write(new StringBuilder().append("export PATH=.:${PATH}:").append(path).append("/JarFiles/tom/bin\n").toString());	
				outCommand.write("make clean \n" + "make all \n");
				outCommand.write("cp "+pathFich+"/"+fichCompilar+" genI \n");
				outCommand.write("cd genI \n");
				outCommand.write("javac gram/Main.java \n");
				
				outCommand.write("java gram/Main < "+fichCompilar+" > res.msp \n");
				outCommand.write("cp res.msp ../genMaqV \n");
				outCommand.write("cd ../genMaqV \n");
				outCommand.write("javac maqv/Main.java\n");

				//fecho da shell
				outCommand.write("exit \n");
				outCommand.flush();

				outCommand.close();
				//int wait = child.waitFor();

				BufferedReader reader = new BufferedReader(new InputStreamReader(child.getInputStream()));
	            String line = "";			
				while ((line = reader.readLine())!= null) {
					System.out.println(line);
				}
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			/* Criacao Threads para Execucao programa C-- e medicao de consumo */
			ThConsumo thc = new ThConsumo();
			ThExec the = new ThExec(inputFichComp);
			the.start();
			thc.start();

			the.join();
			thc.kill();
			thc.join();

			fich.delete();
		}
	} 
}
