import java.io.*;

public class ThConsumo extends Thread {
	private static int intervalo = 2; //milissegundos
	boolean flag = false;
	String nomeFunc;

	public ThConsumo () {
		this.flag = true;
		this.nomeFunc = "Arranque";
	}
	
	public void kill() {
		this.flag = false;
	}
	
	public void run () {
		int tempo = 0;
		int socketNum = EnergyCheckUtils.getSocketNum();
		double[] consAntes = null;
		double[] consDepois = null;
		BufferedReader fichNomeActFunc = null;
		String nomeF = null;

		try {
			fichNomeActFunc = new BufferedReader(new FileReader("MSP/maqv/actualFuncName.txt"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			PrintWriter dram = new PrintWriter("./JFreeChart/consumos/dram.csv", "UTF-8");
			PrintWriter cpu = new PrintWriter("./JFreeChart/consumos/cpu.csv", "UTF-8");
			PrintWriter pack = new PrintWriter("./JFreeChart/consumos/package.csv", "UTF-8");

			EnergyCheckUtils.init();
			consAntes = EnergyCheckUtils.getEnergyStats();

			dram.println(Integer.toString(tempo)+",0,"+this.nomeFunc);
			cpu.println(Integer.toString(tempo)+",0,"+this.nomeFunc);
			pack.println(Integer.toString(tempo)+",0,"+this.nomeFunc);

			while (this.flag) {
				try {
					Thread.sleep(intervalo);

					nomeF = fichNomeActFunc.readLine();
					consDepois = EnergyCheckUtils.getEnergyStats();

					fichNomeActFunc.close();

					if (nomeF != null) {
						this.nomeFunc = nomeF;
					}

					fichNomeActFunc = new BufferedReader(new FileReader("MSP/maqv/actualFuncName.txt"));
				} catch(Exception e) {
					e.printStackTrace();
				}

				tempo += intervalo;
				//Enviar dados para fich csv
				dram.println(Integer.toString(tempo)+","+Double.toString(consDepois[0]-consAntes[0])+","+this.nomeFunc);
				cpu.println(Integer.toString(tempo)+","+Double.toString(consDepois[1]-consAntes[1])+","+this.nomeFunc);
				pack.println(Integer.toString(tempo)+","+Double.toString(consDepois[2]-consAntes[2])+","+this.nomeFunc);

				consAntes = consDepois;
				//System.out.println("dram: " + (consDepois[0] - consAntes[0]) + " cpu: " + (consDepois[1] - consAntes[1]) + " package: " + (consDepois[2] - consAntes[2]));
			}	

			EnergyCheckUtils.profileDealloc();
			dram.close();
			cpu.close();
			pack.close();
			fichNomeActFunc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
