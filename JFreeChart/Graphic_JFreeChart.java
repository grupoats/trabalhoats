import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * This program demonstrates how to draw XY line chart with XYDataset
 * using JFreechart library.
 * @author www.codejava.net
 * Adapted by Ana Sousa
 */

public class Graphic_JFreeChart extends JFrame {

    public Graphic_JFreeChart() {
        super("Green Profiler");
        
        JPanel chartPanel = createChartPanel();
        add(chartPanel, BorderLayout.CENTER);
        
        setSize(640, 480);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    
    private JPanel createChartPanel() {
        String chartTitle = "Consumo de energia ao longo da execução";
        String xAxisLabel = "Tempo (ms)";
        String yAxisLabel = "Consumo (Joules)";
        
        XYDataset dataset = createDataset();
        
        JFreeChart chart = ChartFactory.createXYLineChart(chartTitle, 
                xAxisLabel, yAxisLabel, dataset);

        customizeChart(chart);
        
        // saves the chart as an image files
        File imageFile = new File("consumos/consumo.png");
        int width = 640;
        int height = 480;
        
        try {
            ChartUtilities.saveChartAsPNG(imageFile, chart, width, height);
        } catch (IOException ex) {
            System.err.println(ex);
        }
        
        return new ChartPanel(chart);
    }

    private void readFile (String file, XYSeries serie) {
    	BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {
				String[] values = line.split(cvsSplitBy);
				double time = Double.parseDouble(values[0]);
				double cons = Double.parseDouble(values[1]);
				serie.add(time, cons);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
                try {
	       			br.close();
			    } catch (Exception e) {
                    e.printStackTrace();
                }
            }
		}
    }

    private XYDataset createDataset() {
        XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries dram = new XYSeries("DRAM");
        XYSeries cpu = new XYSeries("CPU");
        XYSeries pack = new XYSeries("Package");
        
        String csvDram = new StringBuilder().append(System.getProperty("user.dir")).append("/consumos/dram.csv").toString();
        String csvCpu = new StringBuilder().append(System.getProperty("user.dir")).append("/consumos/cpu.csv").toString();
        String csvPack = new StringBuilder().append(System.getProperty("user.dir")).append("/consumos/package.csv").toString();

		readFile(csvDram, dram);
		readFile(csvCpu, cpu);
		readFile(csvPack, pack);
        
        dataset.addSeries(dram);
        dataset.addSeries(cpu);
        dataset.addSeries(pack);
        
        return dataset;
    }
    
    private void customizeChart(JFreeChart chart) {
        XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

        // sets paint color for each series
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesPaint(2, Color.YELLOW);

        // sets thickness for series (using strokes)
        renderer.setSeriesStroke(0, new BasicStroke(4.0f));
        renderer.setSeriesStroke(1, new BasicStroke(3.0f));
        renderer.setSeriesStroke(2, new BasicStroke(2.0f));
        
        // sets paint color for plot outlines
        plot.setOutlinePaint(Color.BLUE);
        plot.setOutlineStroke(new BasicStroke(2.0f));
        
        // sets renderer for lines
        plot.setRenderer(renderer);
        
        // sets plot background
        plot.setBackgroundPaint(Color.DARK_GRAY);
        
        // sets paint color for the grid lines
        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.BLACK);
        
        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.BLACK);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Graphic_JFreeChart().setVisible(true);
            }
        });
    }
}

    